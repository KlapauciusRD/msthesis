% !TeX root = ../main.tex
\label{c:film}

Radiochromic film is a practical and flexible dosimeter that enables the user to perform measurements which would otherwise be challenging.
The use of radiochromic film is conceptually straightforward - the film darkens upon exposure to ionising radiation.
The magnitude of this change in film colour is proportional to the dose delivered to the film's active layer.
However, whereas qualitative spatial analysis of dose is simple, accurate quantitative dosimetry is a significant technical challenge.

Given how heavily this thesis relies on film dosimetry for the measurement of skin dose, I have devoted chapter~\ref{c:film} to a detailed discussion of radiochromic film.
This will include a brief description of the dosimetric protocol followed by a recount of all calibrations performed during the course of this research project.
I have endeavoured to make this description clear and practical in order to accommodate the needs of readers engaging in their own experiments or clinical measurements.

In terms of structure, this chapter will include three sections. 
The first section (\ref{s:film_method}) will detail the film dosimetry protocol used, including the choice of experimental method, calibration functions, and uncertainty formalism.
Following this (\ref{s:film_results}), I will present the results of these measurements.
Finally (\ref{s:film_discussion}), I will briefly discuss some of the implications of these results to the overall project.



\section{Film dosimetry protocol}
\label{s:film_method}
Following the review of the literature as it concerns diagnostic energy radiochromic film dosimetry in chapter~\ref{c:bk}, I have collected some of the key recommendations taken from the literature in appendix~\ref{a:protocol}.
These experimental parameters have been collected from a broad selection of published articles, with preference given to articles that provide compelling rationales, and used film in a manner similar to the present study. These recommendations have been used as the basis for designing this calibration and film dosimetry protocol.

\subsection{Calibration experimental arrangement}

The film was irradiated using a GE Optima 660 CT device operating in scout mode.
Rather than rely on the table to support the equipment, an extended arm was constructed.
Using this support, the dosimeters were placed at the CT isocentre.

Total dose for each scout mode setting was measured using a Raysafe X2 R/F solid state dosimeter.
The film was exposed to a known dose by removing the solid state dosimeter and placing the film on a custom-built\footnote{i.e. constructed from toothpicks} phantom (fig.~\ref{f:toothpick_phantom}), then centred using the CT laser guides.


\begin{figure}
	\centering
	\includegraphics[width=.7\linewidth]{dat/pics/toothpick.jpg}
	\caption{Experimental setup for in-air film calibration using scout mode. The film is held at the CT isocentre on a low-scatter phantom. The wooden platform ensures that the film does not move with the table.}
	\label{f:toothpick_phantom}
\end{figure}


%todo model number for film

As the scout mode beam had a collimated width of 5~mm, the solid state dosimeter was placed on an angle so that the active volume lay within the peak dose region.




\subsection{Scanning technique}


I attempted to create a coherent film scanning protocol based on previous work.
This involved considering multiple publications by various authors that were often in partial or complete disagreement.
The film was digitised using an Epson Perfection V370 flatbed document scanner both before and after irradiation.
Each film strip was scanned at 127 dpi into a 48 bit 3-channel tiff image.
All image enhancement options were disabled.


One major divergence from the manufacturer suggestions was scanning each piece of film separately~\cite{Ashland2016}.
A paper mask was attached to the scanner allowing each piece of film to be placed in the same position, with minimal positional uncertainty.
While this introduces some uncertainty due to inter-scan variability, it reduces any uncertainty due to position within the scanner.
This method also forces similarity in the images, which aids automated image processing.


\subsection{Calibration function}

The formalism for calculating dose and uncertainty described by \citeauthor{Tomic2014b} is directly applicable to this study.
This formalism for film analysis is based on the change in reflectance, calculated using equation~\ref{e:reflectance}, where $\Delta R$ represents the change in reflectance due to irradiation, i denotes a film piece from a set of reproduced measurements, $2^{16}$ is the bit depth of the image, and PV denotes the pixel value measured from the red channel of an image.

\begin{equation}
\Delta R^i = R^i_{\text{before}}- R^i_{\text{after}} = \frac{1}{2^{16}}\left[\text{PV}^i_{\text{before}}-\text{PV}^i_{\text{after}}\right]
\label{e:reflectance}
\end{equation}

The film calibration function is fit to the rational function shown in equation~\ref{e:rational_fit}, where $D(\Delta R)$ is the dose delivered to the film, $\Delta R$ is the change in reflectance, and $a$ and $b$ are fitted parameters.

\begin{equation}
D(\Delta R) = \frac{a \Delta R}{1+b\Delta R}
\label{e:rational_fit}
\end{equation}



\subsection{Uncertainty calculation}
\label{sf:uncertainty}

The uncertainty for each data point is taken to be the combination of three factors.
Firstly, the intra-film uncertainty, which is described by the standard deviation in pixel values within a scanned film image.
Secondly, since each data point was repeated three times, the standard deviation between the values of each set of three measurements was used to describe the overall inter-film experimental uncertainty.
Finally, the calibration fit uncertainty must be taken into account when measuring the unknown dose delivered to a piece of film.

The intra-film uncertainty can be formulated according to equation~\ref{e:uncertainty}, in which $\sigma_{\Delta R^i}$ represents one standard deviation of uncertainty for the change in reflectance for a given piece of film, and $\sigma\left(\text{PV}^i\right)$ represents the standard deviation of pixel values in the region of interest before and after irradiation.

\begin{equation}
\sigma_{\Delta R^i} = \frac{1}{2^{16}}\sqrt{\left(\sigma \left( \text{PV}^i_{\text{before}}\right)\right)^2+\left(\sigma\left(\text{PV}^i_{\text{after}}\right)\right)^2}
\label{e:uncertainty}
\end{equation}


A set of film samples for a given measurement point are combined according to equation~\ref{e:weight}, where w represents a weighting factor given by the proportion of the total uncertainty associated with each individual film sample. Using this method, film samples with a higher uncertainty are weighted less heavily, effectively reducing the overall uncertainty of the measurement. This helps reduce the effect of smudges or scanning artefacts on the measured dose. The corresponding uncertainty is aggregated according to equation~\ref{e:uncertaintysum}.

\begin{equation}
\sigma_{\bar{\Delta R}} = \sqrt{\frac{1}{\displaystyle\sum_{i=1}^{3}\left(\frac{1}{\sigma_{\Delta R^i}}\right)^2}}
\label{e:weight}
\end{equation}

\begin{equation}
\bar{\Delta R} = \displaystyle\sum_{i=1}^{3}\left(w^i \Delta R^i\right)
\label{e:uncertaintysum}
\end{equation}

The inter-film uncertainty was taken as the standard deviation for each set of three film exposures.
In the case where only one piece of film is used to measure dose, a conservative estimate of 5\% was used.

Finally, the fit uncertainty was calculated according to the partial derivatives of the calibration function.

\begin{equation}
\frac{\sigma_{\text{fit}}}{D} = \sqrt{\left(\frac{\sigma_a}{a}\right)^2 + \left(\frac{\sigma_b \Delta R }{1+b \Delta R}\right)^2 }
\label{e:fit}
\end{equation}




The overall uncertainty in dose is taken as each of these three components added in quadrature, as shown in equation~\ref{e:total_uncertainty}.


\begin{equation}
\frac{\sigma_{D}}{D} = \sqrt{\sigma_{\text{intra}}^2+\sigma_{\text{inter}}^2+\sigma_{\text{fit}}^2}
\label{e:total_uncertainty}
\end{equation}



\subsection{Data processing}


I analysed the film strips using a Python script (appendix~\ref{a:scripts}).
The script iterates through all images of a film batch, collecting the mean pixel value and associated statistical information within a set region of interest.
Any change in pixel value and standard deviation for each film strip is calculated and scaled by a factor of bit depth to obtain the reflectance.
Film strips providing repeated measurements of the same data point are combined according to a user-generated index file.
Finally, the script uses the indexed data to either generate a calibration function or calculate the dose for each film strip, depending on the purpose of the measurements.

For fitting the calibration function, the parameters a and b were iteratively optimised using an orthogonal distance regression that weights each point according to the associated experimental uncertainty.
Finally, the calibration function generated by this regression is saved for use in future measurements.


There are several alternative approaches to data processing.
ImageJ presents a simple option for measuring average pixel value in a ROI within images.
While this allows the user to proceed without any programming knowledge, it is very slow by comparison.
Additionally, Ashland has published several tools for performing radiochromic film analysis (FilmQA Pro, FilmQA XR).
These programs provide an easy to use interface and allow some useful film analysis techniques (such as triple channel analysis) without requiring comprehensive understanding on the part of the user.

In my choice to develop my own scripts rather than relying on the proprietary options offered by Ashland, I have taken several factors into account.
Most prominently, the commercial solutions require a financial investment that would be difficult to arrange.
Furthermore, developing an in-house solution allowed me to heavily automate image processing and store the results in a format tractable to further analysis.
Finally, I felt that software based on an open source platform that could be used and modified by other users would constitute a useful contribution to the field of radiochromic film dosimetry.







\section{Calibration results}
\label{s:film_results}

The immediate result of the calibration measurements was a set of data describing the relationship between dose delivered and change in reflectance for each peak energy.
These results are shown alongside the fitted rational function in figure~\ref{f:cal_plots}. Table~\ref{t:ab_values} contains a summary of the fit parameters for each beam energy.

The fitted $b$ parameter determines the vertical asymptote of the rational function. 
Since the asymptote is determined by saturation of the film, this implies that the fit appropriately reflects the physical properties of the film.
One notable discrepancy between the expected dose response is the value of $a$ for the 120~kVp fit.
This fails to follow the pattern established by the other curves











\begin{figure}
		\includegraphics[width=\linewidth]{dat/film/cals.eps}
	\caption{Dose delivered to XR-QA2 radiochromic film as a function of measured change in reflectance. Uncertainty in reflectance is given by the standard deviation of measured values, while uncertainty in dose is given by the calibration uncertainty of the reference dosimeter.
	}
	\label{f:cal_plots}
\end{figure}


\begin{table}
	\centering
	\caption{Calibration parameter results for each beam energy obtained using an orthogonal distance regression to a rational calibration function (eq.~\ref{e:rational_fit}). Uncertainty is shown according to the calculations in section~\ref{sf:uncertainty}.}
	\label{t:ab_values}	
	\includespread[template=booktabs,columns = top,file=dat/film/ab_values.ods,range=A2:E5]
	\end{table}








\section{Film dosimetry discussion}

\label{s:film_discussion}
\subsection{Measurement uncertainty}


Figure~\ref{f:cal_unc} shows the experimental, measurement, and total uncertainty for the data points acquired during film calibration.
While this representation is somewhat backward, as these points were used to create the calibration functions, it is representative of the magnitude of uncertainty I expect in future measurements.



\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{dat/film/cal_unc.eps}
	\caption{Experimental, fit, and total uncertainty for the data points associated with each calibration curve. The proportional fit uncertainty increases dramatically as dose increases. The total uncertainty for change in reflectance between 0.2 and 0.35 is below 5\%.}
	\label{f:cal_unc}
\end{figure}

The greatest fit uncertainty for these calibration functions was 1.1 for a, and 0.01 for b.
This corresponds to a dose dependent fit uncertainty of between 4\% and 13\% for the full fit range, increasing as the total dose delivered to the film becomes larger.
Outside of calibration, the highest dose measured during this research project was approximately 70~mGy, which would correspond to a fit uncertainty of 7\%.





\subsection{120 kVp fit}
While the 120 kVp calibration function appears to describe the measured values with reasonable accuracy, it does not conform to the pattern displayed by the other energies.
Previously published work on film dosimetry indicated that the calibration coefficient $a$ increases approximately linearly with beam energy~\cite{Mateus2014}.

The 120~kVp calibration was the first set of experimental measurements performed.
While the calibration protocol did not drastically change, the additional attempts allowed me to refine the technique.
It is possible that poor positioning of the solid state dosimeter led to only partial irradiation during calibration measurements as the device beam in scout mode is comparatively thin.

Unfortunately, due to time pressure the 120~kVp calibration measurements were not repeated.
In deference to the evidence indicating that this calibration curve is illegitimate, I have overridden the the 120~kVp calibration coefficients with the average of those measured for 100~kVp and 140~kVp.
This provides an $a$ coefficient of 87.




\subsection{Calibration range}


%talking about dynamic calibration range

The rational function provides a reasonable fit across the range of calibration dose points.
However, the fitted functions do not accurately calculate dose when film is exposed to radiation levels significantly above the highest calibration points, in this case approximately 240~mGy.
Additionally, increasing the range of the fit by including higher dose points resulted in an inferior fit, particularly at low doses.
Therefore, accuracy can be improved by restricting the dynamic range of the calibration fit.
Since this experiment does not involve in vivo dosimetry, the maximum dose can be controlled by limiting the mAs during exposures.
With this in mind, I estimated that a dynamic range of 0-100~mGy was sufficient for all measurements.





\subsection{Surface measurement coefficient}

As these calibration measurements were performed in free air there are additional considerations for measuring dose on a phantom surface.
Firstly, there will be a small change in energy spectrum based on the difference in energy between the primary beam and the scattered photons.
Secondly, the directional dependence of the radiochromic film will cause it to overestimate dose delivered by scattered radiation.

Based on previous findings, the magnitude of this effect is small~\cite{Mateus2014,McCabe2011}.
As such, the calibration functions will be used as-is for phantom surface measurements.








%key advantages of using my own program:
%data is available for direct use after creation
%non proprietary
%open source
%heavily automated
%at the cost of performing repeated scans, there is not need to select ROIs for each film strip
%It would be difficult to convince footscray hospital to pay for proprietry software
%It would be difficult to convince IT to install proprietry software, even if the hospital pays for it




\subsection{Calibration energy}

Ideally, the beam spectrum used for calibration would match the spectrum of any unknown dose measured using film.
In practice this is challenging, as beam hardening and backscatter affect the overall energy spectrum in an unpredictable manner.
As a result the calibration protocol used here represents a compromise between practicality and accuracy.


The variable effect of beam energy on film response becomes increasingly difficult to quantify when only part of the dose is due to a hardened beam.
Therefore, I have elected to employ the measured calibration functions for any beam generated with a given peak energy.

There are two consolations that may be invoked to qualitatively justify this approximation.
Firstly, in the context of this project any beam that has been hardened by passing through matter is also attenuated.
This reduces the total impact of beam hardening and makes the use of unhardened beams during calibration reasonable.
Secondly, while the effect of backscatter acts to reduce the effective beam energy, beam hardening acts to increase it, somewhat mitigating the overall effect of the phenomena.
Unfortunately, measuring the precise dependence of film response on beam hardness was beyond the scope of this project.

%Overall, this is complicated. Quantifying this effect requires either Monte Carlo simulation or manual exposure.

%I can, however, estimate an upper bound for the uncertainty caused by this effect.




\section{Conclusions}

There are two principle applications for the calibration functions generated during this experiment.
Firstly, film strips may be used as dosimeters by calculating the delivered dose given the change in reflectance for some region of interest.
Secondly, the calibration function may be used to transform the individual pixel values within an image into dose values, creating a set of dose map images.
These two methods allow for a combination of qualitative and quantitative film dosimetry.
Applying these methods will allow radiochromic film to be used to perform surface dose measurements.




