% !TeX root = ../main.tex
\label{c:dose}

Peak skin dose is a dosimetric quantity that correlates to the likelihood of tissue reactions such as erythema.
However, determining peak skin dose delivered during CT is a technical challenge.
In this chapter, I will develop a model for relating the scan parameters and CT device characteristics to the measured dose on the surface of a phantom.





The contributing factors to PSD in CT include scan parameters such as total scan length, as well as device characteristics such as bowtie filter shape.
Dose measured on the external surface of a patient or phantom using radiochromic film corresponds closely to skin dose.
This allows physical measurements with radiochromic film to be used in order to quantify the impact of each contributing factor.



Structurally, this chapter will begin with a section (\ref{sd:theory}) dedicated to discussing the physical properties of each factor identified as having a significant, measurable impact on peak skin dose.
Where applicable, I will outline a formula that approximates the physical effect of the phenomena.
Following this (\ref{sd:method}), I will outline the experimental and numerical methods used to quantify the relationship between surface dose and CTDIvol.
The results section~(\ref{sd:results}) will show the measured data, fitted conversion factors and demonstrate the overall agreement between the model and measured data.
Finally (\ref{sd:summary}), the form of the model will be presented alongside some concluding statements.

\section{Theory}
\label{sd:theory}
\subsection{Beam energy}

The energy spectrum of an x-ray beam has wide ranging effects on its behaviour.
High energy beams are more prone to scattering and more penetrative.
Furthermore, beam energy steadily increases with path length through matter due to beam hardening.

In order to account for the effect of energy on each aspect of calculating surface dose, I have broken several fit parameters into two components, as shown in equation~\ref{e:energy}.
Using this approximation, a fit parameter $A$ has a constant component $A_c$ and a variable component that increases linearly with peak energy (kVp), $A_v$.
The peak energy is normalised so that it falls between 0 and 1 in the range 80 to 140~kVp.


\begin{equation}
A = A_c + A_v\frac{kVp-80}{60}
\label{e:energy}
\end{equation}




\subsection{Scan length}

%The factor describing scan length must relate the change in dose due to increased total size 

%Beam collimation and total scan length both contribute to the relationship between CTDIvol and skin dose.
%However, the impact of these phenomena is due to fundamentally different mechanisms.

Scan length directly increases surface dose for a given CTDIvol.
As the total length increases, skin dose at a given point will increase due to scatter from the neighbouring regions.
The contribution to dose due to scatter from a point decreases with distance and attenuating material.
As such, contribution from nearby regions will be high, while distant regions will contribute less to local surface dose.


%Previously, the effect has been modelled using equation~\ref{e:klength}, where $k_{\text{length}}$ represents the effect of scan length, l represents the scan length, and m represents a fitted value that quantifies scatter.
%
%\begin{equation}
%k_{\text{length}} = 1-e^{-ml}
%\label{e:klength}
%\end{equation}


Several models describing the increase in scatter due to increased scan length have been suggested.
~\citeauthor{Mclean2016} proposed an exponential fit, which had good agreement with physical measurements up to a scan length of 40~mm~\cite{Mclean2009}.
However, this method did not adequately describe the measured data for longer scan lengths.
I have employed equation~\ref{e:klength}, a modified version of the previously used exponential correction factor.
This formula depends on the total scan length $l_{scan}$ and a pair of fitted values, represented by $A$.
This equation increases rapidly at low lengths, but reaches an asymptote more slowly as scan length increases.




\begin{equation}
k_{\text{length}} = 1-\exp(-\sqrt{l_{scan}}*A)
\label{e:klength}
\end{equation}


Conceptually, this correction for total scan length assumes that the entire scan is composed of one beam.
As such, this factor applies directly when scan length is equal to beam collimation, but requires an additional correction factor otherwise.







\subsection{Beam collimation}


Following from the strategy used to define the scan length correction factor, the beam collimation factor must correct the peak surface dose due to one large slice into dose due to a series of adjacent slices.


Wasted dose and beam divergence are necessary to describe the effect of beam collimation on skin dose.
Wasted dose refers to the proportion of the beam which falls outside of the detector array.
This buffer region is necessary in order to keep the dose within the main field approximately constant.

%Because the buffer required is similar in size regardless of total beam collimation, the proportion of radiation inside and outside the main field changes with beam collimation.
%Since the length by which the radiation field must exceed the detection area is constant, the proportion of wasted dose is lower for wider fields.

The z width of the beam increases steadily as it passes through the phantom due to the divergence of the CT beam.
This leads to areas of high skin dose corresponding to the centre of each axial slice, with lower dose areas between slices.
%This geometry is represented in figure~\ref{f:divergence}.%todo picture showing peak skin dose as beam enters





Drawing together the idea of wasted dose and beam divergence: while wasted dose contributes to CTDIvol, it tends to fall into low dose areas.
Therefore, while wasted dose contributes to CTDIvol, it tends not to contribute to PSD.
The effect of wasted dose the relationship between CTDIvol and PSD increases as the beam collimation decreases.
For a given scan length, the effect of beam collimation on the relationship between CTDIvol and PSD has been modelled based on the number of axial rotations used.





%This physical scenario is described by equation~\ref{e:kcoll}, where $l_0$ is the beam collimation, and A is the effective length of the wasted dose region.
%
%\begin{equation}
%k_{\text{coll}} = \frac{l_0}{l_0 + A}
%\label{e:kcoll}
%\end{equation}

The effect of beam collimation on total scan length can be approximated using equation~\ref{e:kcoll}, where $l_{coll}$ is the beam collimation, $l_{scan}$ is the total scan length, and $B$ represents a pair of fitted constants.
This equation depends on the total number of axial equations, which is equal to the ratio $l_{scan}/l_{coll}$.
The logarithmic dependency on the number of rotations accounts for the large impact of the closest low dose regions, but a smaller impact due to the farthest low dose regions.



\begin{equation}
k_{\text{coll}} = \frac{1}{1+\ln(l_{scan}/l_{coll}) / B}
\label{e:kcoll}
\end{equation}


%In order to demonstrate to applicability of these models and to determine fitted values, physical measurements are necessary.


%The first aspect of investigating the impact of beam collimation on the relationship between CTDIvol and skin dose was disentangling the effects of total scan length and physical beam width.
%In order to test the hypothesis that total scan length is the governing factor, I performed a series of skin dose measurements using repeated total scan lengths achieved using a variable number of rotations.
%If beam collimation can be included as a function of scan length, the result of this test would show a constant relationship between CTDI and peak skin dose for the chosen scan lengths.


















\subsection{Patient size and geometry}

Accounting for patient size in CT is confounded by the rotational geometry of CT.
For example, an increase in patient size reduces the minimum source-surface distance (SSD) while increasing the maximum SSD.
Furthermore, patient size affects the path length through the patient and angular offset of a given point to the beam central axis.
A representation of these geometric concerns is shown in figure~\ref{f:ctgeo}.


\begin{figure}[t]
	\centering
	\includegraphics[width=.6\linewidth]{dat/dose/ctgeo.eps}
	\caption{CT geometry showing the primary geometric factors that affect patient skin dose at the red cross during CT irradiation. The factors are total path length (D), path length through phantom (d), and angular offset ($\phi$).
	}
	\label{f:ctgeo}
\end{figure}

For each position on the gantry, the total path length will affect the skin dose by a factor of $\frac{1}{D^2}$. Therefore, the resulting dose contribution will decrease rapidly from a maxima near the minimum SSD.


Furthermore, beyond a given critical angle the beam must also pass through the patient before reaching the same point on the patient surface.
As the beam passes through the patient, it will attenuate in overall intensity.
This attenuation will deviate slightly from an exponential attenuation due to the effects of beam hardening.


Finally, the angle from the beam central axis to the surface point will change as the source rotates.
Since the thickness of the bow tie filter varies with angular offset, the beam will drop in intensity away from the central axis.



However, an analytical solution to how these variables affect dose did not present itself.
I suspect that, even if it did, it would take a form too complex for regular use.
Therefore, a numerical approach seems prudent.


\subsection{Helical scans}


Helical mode introduces three confounding factors into the relationship between CTDIvol and skin dose.
The first and most prominent is the effect of pitch.
The second major change introduced by helical mode is the requirement for overranging.
These geometric concerns are represented graphically in figure~\ref{f:helical}.
Finally, an added complication for helical scans is that they do not begin and end with the source in a fixed position.



\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{dat/skin/helical}
	\caption{Two scans with the same beam collimation and scan length in helical and axial mode.
		The rectangular areas represent the flattened surface of a cylindrical phantom, with high dose regions shown in red.
		In helical mode, the high dose areas form a smaller proportion of the phantom surface when the pitch is greater than 1.
		The additional length of the helical scan is due to overranging.}
	\label{f:helical}
\end{figure}

Pitch describes how far the table travels for each source rotation, relative to the beam collimation.
For a pitch of 1 the table will travel a distance equal to the beam collimation for every source rotation, which is equivalent to the number of rotations in an axial scan.
Increasing pitch is a dose efficiency measure that typically reduces the overall patient dose.
However, for a given volumetric dose, reducing the dose entrance area means the dose will be concentrated within the smaller region.
Therefore, while using a high pitch reduces overall dose there will be more dose delivered to high dose regions for a given CTDIvol.

For helical scans, the ratio of high dose area to total surface area is inversely proportionate to pitch.
As a result, the dose delivered to the surface is concentrated by a factor equal to the pitch.





Over-ranging leads to an increase in total scan length proportional to the beam collimation~\cite{Schilham2010}.
To compensate for this, I have increased the effective scan length when using helical mode by the distance covered during a single source rotation multiplied by a device-specific efficiency parameter.
This increase in total scan length is described by equation~\ref{e:helical}, where $l_{scan}$ is the original scan length, $p$ is the pitch, $l_{coll}$ is the beam collimation, and $e$ is a device-specific value that mitigates the effect of over-ranging.

\begin{equation}
l_{\text{helical}} = l_{scan} + p\cdot l_{coll} \cdot e
\label{e:helical}
\end{equation}

%Modern devices have endeavoured to reduce some of this wasted dose.
%For example, a dynamic collimator can reduce the impact of collimators by up to 50\%.
%The potential for mitigating wasted dose during helical scans is described by the constant $e$.






The arbitrary location of the CT source at the beginning of the scan does not dramatically affect surface dose during a single scan.
It does, however, make measuring surface dose more difficult as longer strips of radiochromic film are required.
Furthermore, the peak dose region for sequential scans may overlap entirely, partially, or not at all.
Therefore, while it is possible to predict the maximum and minimum surface dose for a series of helical scans, the actual delivered dose depends on the random timing of the system.
Despite this, a worst case scenario estimate corresponding to complete overlap between multiple scans is possible.






\section{Method}
\label{sd:method}
\subsection{Surface dose measurements}

Surface dose measurements using radiochromic film were taken in order to interrogate the underlying processes.

Radiochromic film was used for quantifying the effect of scan length and beam collimation on peak surface dose delivered to a CTDI phantom.
Scan lengths of 40~mm and 120~mm were employed.
These scan lengths were achieved using 40~mm, 20~mm, and 10~mm axial beam collimations.
The scans were repeated at beam energies of 80~kVp, 100~kVp, 120~kVp, and 140~kVp.
The machine reported CTDIvol and surface dose at the top of a 32~cm diameter CTDI phantom was recorded for all permutations of these parameters.
The experimental set up for these measurements is shown in figure~\ref{f:ctdipic}
Due to time constraints, the data points for 80~kVp and 100~kVp were each taken using a single measurement, reducing their statistical power.

\begin{figure}
	\centering
	\includegraphics[width=.6\linewidth]{dat/pics/ctdi.jpg}
	\caption{Radiochromic film waiting patiently to be exposed on the top surface of the CTDI body phantom.}
	\label{f:ctdipic}
\end{figure}


For most exposures, a 1.5~by 2.5~cm piece of radiochromic film was placed on the top surface of a 32~cm diameter CTDI phantom.
For several 120~mm scan lengths, longer film strips were used in order to facilitate qualitative analysis of the beam profile.
The centre of the film was aligned with the two vertical laser planes after centring the phantom at the CT isocentre.
The surface dose was calculated according to the protocols established in chapter~\ref{c:film}.

\subsection{Consistency}

For each day of measurements involving the CTDI body phantom two pieces of radiochromic film were exposed with a consistent protocol.
The exposure was made using a single 40~mm axial rotation at 120~kVp and 200~mAs, giving a CTDIvol of 17.4~mGy.
The consistency in the dose measured using this exposure was used as a metric for assessing the magnitude of day-to-day variation in measured dose.


\subsection{Function fitting}

The fitted values for the conversion factors governing scan length and beam collimation were obtained with a single orthogonal distance regression.
The fit included only data points taken on the top surface of the 32~cm CTDI phantom.





\subsection{Bowtie filter}
The attenuation profile due to the bowtie filter for the GE Optima 660 was measured using a strip of radiochromic film.
The film was situated horizontally at the CT isocentre and exposed for 3~s at 120~kVp and 200~mA.
The resulting image was converted to a dose profile and measured using ImageJ.
The profile was smoothed using a wide Savgol-Golar filter in order to mitigate the effect of smudging and scanning artefacts on the final calculations.



\subsection{Depth dose}

It was not possible to physically measure the depth dose curves for the GE Optima 660 scanner without full access to manual exposures.
Instead, the depth dose data was measured on the CT component of a Siemans MIYABI Angio-CT.
The depth dose profiles were measured using radiochromic film suspended vertically in a container of water.
The submerged film was then exposed in manual mode using a top-down beam.
This arrangement is shown in figure~\ref{f:ddmethod}.



\begin{figure}
	\centering
\includegraphics[width = .6\linewidth]{dat/dose/ddgeo.eps}
	\caption{Experimental arrangement for measuring depth dose using radiochromic film.}
	\label{f:ddmethod}
\end{figure}

Following exposure, the film was scanned and processed into dose profiles.
The dose profile was corrected according to the inverse square of distance from the source.
Finally, the profile were filtered to remove noise and artefacts.







\subsection{Patient size}

Patient size correction was performed using a numerical approach.
This approach involved calculating the geometric parameters then using them to estimate dose for a given experimental scenario.



%calculate the three parameters as a function of angle and patient size
First, the total distance, path length through matter, and angular offset were calculated for each possible source position.
I defined the phantom as an ellipse with an anterior-posterior diameter and lateral diameter, which allows a first order approximation of a human torso.
The midline of the ellipse on the anterior surface is designated as the point of interest.

The source was mathematically defined by a vector originating at the CT isocentre:

\begin{equation}
\tilde{S} = \left(R\sin(\theta),R\cos(\theta)\right)
\end{equation}

The total distance D is then given by the difference between the vector $\tilde{S}$ and the vector $\tilde{P}$ which denotes a point on the upper surface of the phantom geometry:

\begin{equation}
D =|\tilde{D}| = |\tilde{S}-\tilde{P}|
\end{equation}

The offset angle phi follows by use of the cosine rule:

\begin{equation}
\cos(\phi) = \frac{\tilde{D}\cdot \tilde{S}}{|\tilde{D}||\tilde{S}|}
\end{equation}

Finally, the distance $d$ is calculated as the proportion of the vector $\tilde{D}$ which lies within the defined phantom shape. See appendix~\ref{a:scripts} for details.



%attenuation
The attenuation due to interactions with matter was estimated by calculating an effective total path length through water.
The distance through the phantom contributes to this directly, while the bowtie attenuation and table contribute indirectly.
The bowtie filter profile was used to generate an effective path length through water at 120~kVp.
A constant path length was added for any source angle that passes through the table before reaching the designated point on the phantom surface.
Combining the path length due to these factors allows a total effective path length to be estimated, which in turn allows the overall attenuation in beam attenuation to be predicted.




%Estimate dose contribution from each angle
Using these measurements, the proportional dose was calculated according to equation~\ref{e:dose_size}, where $D_\text{skin}$ is the proportional skin dose, $D$ is the total path length, and DD is the depth dose function for a given beam kVp and effective path length through water $d_{\text{eff}}$.


\begin{equation}
D_\text{skin}(\theta) \propto \frac{1}{D(\theta)^2} \text{DD}\left(d_{\text{eff}}(\theta),\text{kVp}\right)
\label{e:dose_size}
\end{equation}


The skin dose due to a single rotation can be measured by integrating the dose contribution for a full rotation:

\begin{equation}
D_{\text{skin}} \propto \int_0^{360} D_\text{skin}(\theta) d\theta
\end{equation}

Finally, the size correction factor is calculated by normalising the result of this integral to calculated skin dose to the 32~cm diameter CTDI body phantom exposed to radiation of the same energy:

\begin{equation}
k_{size} = \frac{D_\text{skin}(\text{size, kVp})}{D_\text{skin}(\text{32 cm circ., kVp})}
\end{equation}


%Repeat for different patient sizes
Shapes representing the head CTDI phantom, body CTDI phantom, Catphan\textsuperscript \textregistered~600, and approximating a human chest were investigated, allowing direct comparison between the model and physical measurements.
Finally, the calculations were repeated for shapes of varying size and eccentricity, creating a continuous relationship between CTDIvol and surface dose over a range of normal patient sizes.

The calculated values were compared to surface dose measured using radiochromic film.
Measurements on cylindrical phantoms of diameter 16, 20, 32 and 40~cm were used to assess the predictive power of this model.
For these measurements, the dose was recorded on the top of the phantom using radiochromic film.
%Validation exposures were performed by comparing film exposed on the top surface of the 16~cm diameter CTDI phantom and phantoms of other shapes and sizes.
%Due to the limited availability of physical phantoms, this included the 8~cm diameter CTDI head phantom, 10~cm diameter CATphan, %todo other sized phantoms

\subsection{Helical scans}

Helical mode with a scan length of 40~mm was used to expose film on the top surface of the CTDI body phantom.
For each energy, a measurement was taken with 20 and 40~mm collimation, leading to a total of 8 helical mode data points.
Due to the large amount of film needed to reliably capture helical mode scans, each data point consisted of a single measurement.








\section{Results}
\label{sd:results}


\subsection{Surface dose}
The film strip in figure~\ref{f:dose_shape} shows the pattern of high dose and low dose regions on the phantom surface.
This film was exposed to 3 consecutive 40~mm axial slices.
I found the shape of the skin dose profile for sequential axial CT slices somewhat unexpected.
In particular, I mistakenly expected skin dose to be relatively uniform on the phantom surface.
The skin dose profiles show a dose maxima around the centre of each slice, with a low dose region between slices.
This concentration of skin dose at the centre of axial slices is a consequence of the diverging nature of a CT x-ray beam.



\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{dat/skin/dose_shape.png}
	\caption{Radiochromic film exposed with 40~mm beam collimation and 120~mm scan length.
		The beam collimation at the phantom surface is less than the axial slice thickness, which creates a pattern of low and high dose regions.}
	\label{f:dose_shape}
\end{figure}


\subsection{Consistency}

A consistent exposure protocol was used to measure surface dose during each day of measurements in order to estimate variation between experiments.
The standard deviation across this set of measurements was 2.7\%, which is similar to the typical uncertainty within a set of three consecutive measurements.
This implies that film response variation between sessions was not significantly affected by factors such as temperature, delivered dose, delay in digitising samples, and optical scanner response.





\subsection{Beam collimation and scan length}
%results


Peak surface dose measured using radiochromic film and then divided by the device-reported CTDIvol is shown in figure~\ref{f:ctdidata}.
This shows the expected increase in PSD/CTDIvol as either scan length or beam collimation increases.
There is a slight apparent energy dependence, with lower surface dose delivered when using higher energy beams.
There is one point at 20~mm collimation, 40~mm scan length, and 140~kVp that does not appear to conform to the pattern established by the other points.
This discrepancy could be simply due to a clerical error; the point was not used in any further calculations or regressions.


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{dat/skin/ctdidata.pdf}
	\caption{Measured dose/CTDIvol across a range of energies, scan lengths and beam collimations.}
	\label{f:ctdidata}
\end{figure}



The fitted conversion factor for scan length is shown in figure~\ref{f:klength}.
This plot reflects equation~\ref{e:klength}, where the fitted values $A_c$ and $A_v$ are 0.0219 and -0.0066.
The measured data points for which the beam collimation was equal to the scan length are also shown on this graph, as for these points there are no other conversion factors to apply.
The form of the correction shows several desirable behaviours that correspond to the physical effect of increasing scan length.
Specifically, the conversion factor increases rapidly for short collimations and tapers off for high collimations.



\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{dat/skin/klength.pdf}
	\caption{The conversion factor for scan length ($k_{\text{length}}$) for each beam energy. The fitting parameters $A_c$ and $A_v$ are 0.130 and -0.018.}
	\label{f:klength}
\end{figure}





The conversion factor for beam collimation describes the effect of increasing the total number of axial slices.
The fitted beam collimation correction factor, $k_\text{coll}$, is plotted against the number of axial rotations for each beam energy in figure~\ref{f:kcoll}.
The fitted parameters $B_c$ and $B_v$ are 7.23 and 0.89.
This function shows the expected decrease in the relationship between surface dose and CTDIvol as the number of collimations for a given scan length increase.
While $k_\text{coll}$ changes rapidly for the first divisions, the effect tapers off as the number of slices for a given scan length increases.


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{dat/skin/kcoll.pdf}
	\caption{The conversion factor for beam collimation ($k_{\text{coll}}$) for each beam energy. The fitting parameters $B_c$ and $B_v$ are 7.23 and 0.89.}
	\label{f:kcoll}
\end{figure}



\subsection{Bowtie filter profile}

The attenuation due to the bowtie filter is plotted in figure~\ref{f:bowtie}.
This shows the expected decrease in beam intensity with increasing angular offset.
However, there is also a decrease in beam intensity around the beam central axis.
This indicates that the field intensity is greatest at 3$^{\circ}$ angular offset for the head filter and 6$^{\circ}$ offset for the body filter.
There is less baseline filtration used for the head filter which explains the increase in overall intensity in comparison to the body filter.



\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{dat/skin/pathlength/bowties.eps}
	\caption{Bowtie filter intensity profile for the head and body filters, showing relative intensity as a function of angular offset from the central axis.}
	\label{f:bowtie}
\end{figure}

\subsection{Depth dose profiles}

The relative measured dose as a function of depth in water is shown in figure~\ref{f:ddprofile}.
This shows a slow decrease in intensity as the scatter contribution builds up near the surface of the water followed by near-exponential decay as the beam travels deeper within the liquid.




\begin{figure}[t]

	\centering
	\includegraphics[width=\linewidth]{dat/skin/dd.eps}
	\caption{Measured dose as a function of depth in water for beam energies of 100, 120, and 140~kVp. Dose is  corrected for the reduction in dose due to distance from the X-ray source.}
		\label{f:ddprofile}
\end{figure}

The dose at each point was corrected according to the distance from the X-ray source.
This separated the effect of geometric decrease in beam intensity due to SSD from attenuation and scatter.
Using this correction allows the depth dose profile to be used to estimate attenuation despite a variable SSD.
This type of correction ignored the effect of geometric divergence on scatter contribution.
However, given that SSD will likely remain within the range of 30-40~cm, this was sufficient for the purpose of this project.


\subsection{Patient size results}

The geometric data for a set of representative phantom geometries is shown in figure~\ref{f:geos_doses}.
In particular, this shows the 32~cm diameter CTDI phantom, 16~cm diameter CTDI body phantom and an example of two ellipsoid phantoms.
The geometric plots show the behaviour of the parameters d, D and $\phi$ as the source is rotated around the gantry.
The total distance D conforms to my expectations, starting at a minimum value then increasing to a maximum at 180$^{\circ}$.
Similarly, the angle $\phi$ increases rapidly at the beginning of the rotation then decreases back to 0 at 180$^{\circ}$.
The final curve represents the path length through the phantom, d.
This length is zero until a critical angle that is determined by the size and shape of the phantom.
At this point, the path length through the phantom increases.
In the case of an ellipsoidal phantom the plot is somewhat distorted around 180$^{\circ}$, depending on the orientation of the phantom.

The total contribution to dose as a function of angle shows that dose is primarily due to the closest 200$^{\circ}$ arc of the source.
This is particularly true of large phantoms, where the dose is due to a smaller, closer portion of the overall source arc.
For smaller phantoms, there is a meaningful contribution from a wider range of source angles.
The 16~cm phantom indicates that for the CTDI head phantom, there is a significant contribution to dose for source angles up to around 150$^{\circ}$.
Furthermore, the effect of the bowtie filter reduces the effect of midline exposures. For this reason, the dose contribution reaches a maximum at between 15$^{\circ}$-30$^{\circ}$.



\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{dat/skin/pathlength/geos_doses.eps}
	
	\caption{Total path length D, path length through phantom d, angular offset from central axis $\phi$, and estimated dose contribution as a function of source angle. The values shown are calculated at a point on the top surface of the designated phantom.}
	\label{f:geos_doses}
\end{figure}






The relationship between phantom size and skin dose is shown in figure~\ref{f:totdose_80}.
This plot shows the effective total beam intensity at the top surface of the phantom, normalised to have a value of 1 for a 32~cm diameter CTDI phantom.
This plot represents the size correction factor for a given cylindrical or ellipsoid phantom relative to the CTDI body phantom used for previous measurements.


The size correction factor has a minima around 46~cm AP diameter.
Furthermore, there is relatively little difference between phantoms of diameter 34-60~cm.
Practically, this means that within the normal range of patient sizes there will be an overall effect of within approximately 10\%.
However, for very small or large patients peak skin dose depends heavily on size.




\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{dat/skin/pathlength/totdose_120.pdf}
	\caption{Beam intensity at point on phantom surface for a cylindrical and ellipsoidal phantom at 80~kVp. Normalised to the surface dose for a 32~cm diameter cylindrical phantom. This quantity is the beam energy dependent correction factor $k_{\text{size}}$. The shaded regions correspond to the range of typical head and body sizes.}
	\label{f:totdose_80}
\end{figure}
%todo I want all these plots. Because why not.


This determination of $k_{\text{size}}$ shows that for large phantoms the eccentricity of the ellipse has minimum impact on total skin dose.
However, at low phantom sizes the eccentricity has a greater impact on the dose.
The ratio between ellipsoid and circular phantoms is plotted in figure~\ref{f:shaperatio}.
This indicates that the difference between circular and eccentric phantoms peaks at around 14~cm AP diameter and then steadily decreases.
Fortunately for the ease of using these results clinically, smaller body parts tend to be less eccentric than larger ones.
For typical torso sizes of 30-50~cm AP diameter, eccentricity causes less than a 5\% difference in dose.

\begin{figure}[tb]
	\centering
	\includegraphics[width=\textwidth]{dat/skin/pathlength/shaperatio.eps}
	\caption{Ratio of top surface dose between cylindrical and ellipsoid phantom for beam energies of 80, 100, and 120 kVp. }
	\label{f:shaperatio}
\end{figure}



%
%The principle outcome of these calculations is the ratio between dose to a 16~cm radius cylindrical phantom and dose to an ellipsoidal phantom of arbitrary size. Table~\ref{t:size_ratio} shows a summary of physical measurements used to validate this model. Notably, the predicted and measured ratios typically differ by less than %todo need more model validation here
%
%\begin{table}
%	\caption{Measured vs predicted skin dose on phantom surface.}
%	\label{t:size_ratio}
%	\centering
%	\includespread[template=booktabs,columns = top,file=dat/skin/size_test.ods,range=A2:F2]
%\end{table}


\subsection{Helical results}

The ratio of measured to predicted surface dose delivered during a series of helical scans is shown in figure~\ref{f:helical_results}.
The stated efficiency factors correspond to how far the system over-ranges the designated scan length in order to allow image reconstruction.
An efficiency factor of 100\% would correspond to one full additional rotation, while a factor of 0\% corresponds to no wasted dose due to over-ranging.
The uncertainty in these data points has been omitted for graphical clarity.
Since these data points were acquired with a single measurement, the estimated uncertainties are 6-8\%.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{dat/skin/hel.pdf}
	\caption{Ratio of measured/predicted PSD delivered during 40~mm scan length helical acquisitions for 20 and 40~mm beam collimation. Efficiency factors for calculating $k_{helical}$ of 0\%, 50\%, and 75\% are shown. }
	\label{f:helical_results}
\end{figure}

Due to the inherent randomness of helical scans the length of over-ranging is difficult to measure without an experiment designed for this purpose, the need for which was discovered too late for it to be acquired.
However, physically measuring the extent of over-ranging on the exposed films indicated that the total additional scan length is approximately between 50\% and 75\% of one helical rotation.
Based on these results, I have estimated the efficiency factor used to mitigate the effect of over-ranging to be 50\%.
Users with newer devices may wish to use an efficiency parameter that further reduces the additional scan length caused by overranging.











These measurements indicate that the core assumptions used to estimate surface dose in helical mode are reasonable.
However, the helical data points represent only a small subset of possible scans.
Further, the data points used present an exaggerated example of over-ranging, as typically helical scans are considerably longer in which case the proportion of wasted dose is much smaller.





\subsection{Explanatory power}

In figure~\ref{f:datavsmodel} I have compared the measured data to the predicted surface dose calculated using this model.
This provides a visual metric for the overall agreement between the proposed model and the actual surface dose delivered during these experiments.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{dat/skin/datavsmodel.pdf}
	\caption{Ratio of measured/predicted PSD for all axial data points measured during the course of this project.}
	\label{f:datavsmodel}
\end{figure}

With few exceptions, the data points measured on the 32~cm CTDI phantom fall within uncertainty of the model's predicted value.
That being said, there does appear to be a tendency to overestimate dose at low scan lengths.
However, the small number of data points acquired with 20~mm scan length makes it difficult to assess the model reliably in this regime.

The data points for phantom sizes other than the 32~cm CTDI phantom show a clear systematic bias between measured and predicted dose.
For both the smaller and larger phantoms, the size correction factor $k_{\text{size}}$ partially compensates for the size change.
In both cases the magnitude of the correction is too low by around 10\%.
One possible source for this discrepancy is the use of non-device specific depth dose data.






\section{Model summary}
\label{sd:summary}
\subsection{Model formalism}

The summarised formalism developed for this model is represented in equation~\ref{e:formalism}.
The formula for each of the contributing conversion factors is revisited in equation~\ref{e:form1}.
The fitted parameters for $k_{\text{length}}$, $k_{\text{coll}}$, and the residual constant $c$ are summarised in table~\ref{t:fits}.



\begin{equation}
\label{e:formalism}
PSD = \text{CTDI}_{\text{vol}}
\cdot k_{\text{coll}}
\cdot k_{\text{length}}
\cdot k_{\text{size}}
\cdot k_{\text{pitch}}
%\cdot k_{\text{pitch}}
\cdot c
\end{equation}


%\cdot \cosh(-mpO), \text{for proportional offset along scan

\begin{align}
\begin{split}
k_{\text{coll}} &= \frac{1}{1+\ln(l_{scan}/l_{coll}) / B}\\
k_{\text{length}} &= \begin{cases}
1-\exp(-\sqrt{l}*A), \text{if axial mode}\\
1-\exp(-\sqrt{l_{scan}+p\cdot l_{coll}\cdot e}*A), \text{if helical mode}
\end{cases}\\
k_{\text{size}} &\propto \int_{0}^{360} \frac{1}{D(\theta)^2}DD(\theta)BT(\theta)d\theta\\
k_{\text{pitch}} &= \begin{cases}
1, \text{if axial mode}\\
p, \text{if helical mode}
\end{cases}\\
\end{split}
\label{e:form1}
\end{align}



\begin{table}
	\centering
	\caption{Summary of fitted parameters for the GE Optima 660 CT scanner.}
	\label{t:fits}
	\begin{tabular}{llll}
		\toprule
	conversion factor&Parameter & Constant & Variable \\ \midrule
	$k_{\text{length}}$ &A&  0.130 & -0.019\\
	$k_{\text{length}}$ &B& 7.27 & 0.89 \\
	$l_{helical}$ & e & 0.5 &- \\
	Residual constant & c & 2.53 & - \\ \bottomrule
	\end{tabular}
\end{table}

\subsection{Conclusions}

This model allows a value for surface dose to be calculated for any combination of beam collimation, scan length, filter, scan mode, kVp, and phantom size.
While the model shows good accuracy when used on the 32~cm CTDI body phantom, the results for other phantom sizes are less consistent.

