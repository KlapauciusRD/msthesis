% !TeX root = ../main.tex
\label{c:val}

%Model analysis


This chapter is primarily concerned with assessing and employing the model developed in chapter~\ref{c:dose}.
This includes describing the merits and limitations of the model, as well as employing the model to perform some basic estimations.
A series of skin dose measurements taken using an anthropomorphic phantom will be used to demonstrate the utility of the model in a clinically relevant scenario.
Where published values exist in the literature I will attempt to apply the current model and provide a surface dose estimate, discussing any agreements and discrepancies.
Additionally, I will describe the steps necessary to utilise the model on other  CT devices.


Structurally, this chapter will begin with a discussion of the proposed model's limitations and avenues for improvement~(\ref{sv:limit}).
In the second section of this chapter~(\ref{sv:anthro}) I will test the ability of the model to predict surface dose delivered to an anthropomorphic phantom.
In the third section (\ref{sv:delas}) the results of this project will be compared to values previously published by \citeauthor{DelasHeras2013}.
Following this (\ref{sv:implement}) I will describe the process of employing the model, as well as the procedure for acquiring device-specific source data.
%Finally (\ref{sv:conc}), I will make some concluding remarks and discuss the future of CT skin dose.








%Limitations of the model
\section{Limitations of the model}
\label{sv:limit}

Like any physical model, the method for calculating skin dose developed in this thesis is a mathematical approximation to a physical reality.
While models are a useful tool, using a model outside of its intended regime can be unreliable.
It is prudent to be cautious and understand the limitations of a model before employing it.
In this section I will discuss the limitations of the model and potential avenues for overcoming these limitations.


The model outlined in this thesis is designed for use in a dual-modality Angio-CT environment.
Some of the assumptions and simplifications underpinning the model break down as the scan parameters diverge from this regime.
In particular, the model does not fully account for beam hardening, the possibility of overlapping scans, cross dependence between scan parameters, or table attenuation.
Furthermore, while the model allows for the use of device specific data it was not possible to measure this data to my satisfaction.



\subsection{Beam hardening}


Beam hardening refers to the increase in average photon energy for a spectral radiation beam as it penetrates matter.
While beam hardening is partially accounted for through the use of measured depth dose profiles, there are other avenues by which it may affect the estimation and measurement of skin dose.
Firstly, radiochromic film is somewhat energy dependent.
As energy increases, the film response relative to water decreases.
When using a calibration function created for in-air exposures, this will lead to an underestimation of dose.
This underestimation will increase with beam hardness.
Therefore, in scenarios in which a larger portion of the total dose is partially attenuated, this phenomena will have a greater effect.

The magnitude of this effect could be explored using a series of calibration-type measurements taken in an attenuated beam.
This could, in theory, be explored using a CT device with manual exposures through increasing layers of perspex.


One avenue for exploring this phenomena would be measuring dose with both film and a tissue-equivalent dosimeter while increasing attenuation.
By taking repeated measurements behind increasing thicknesses of perspex, the response of film as a function of beam hardness could be measured.
However, quantification of this effect was not possible for the present project as the manual exposure mode on the CT device used in most measurements was not accessible.









\subsection{Scan overlap}


In the case of thin collimation and small phantom sizes, it is possible for the high dose region from adjacent axial slices to overlap.
This phenomena is represented in figure~\ref{f:overlap}, which shows the peak surface dose profile along the z-axis for a CTDI head phantom scanned with a series of 10~mm axial slices.
While the model predicts the dose at the centre of each slice with reasonable accuracy, the dose at the intersection between the two slices is underestimated.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\linewidth]{dat/val/overlap.eps}
	\caption{Surface profile along the z axis delivered using a beam collimation and phantom size that allows (left) and restricts (right) overlap between adjacent slices. In this case, the peak dose delivered to the overlapping regions is greater than the surrounding region by a factor of 1.4.
	}
	\label{f:overlap}
\end{figure}

Overlap is affected by two phenomena.
Firstly, the size of the beam increases to reach the stated collimation at the CT isocentre.
For a beam that is perfectly collimated to the detector array, this means that the size of the beam at the surface of the phantom is proportional to the SSD.
It follows that the size of the low dose regions for a perfectly collimated beam will decrease to zero at the CT isocentre.
However, the CT beam cannot be perfectly collimated.
There is, by necessity, a region of over-irradiation.
Therefore, the over-beam region will cross over into the next slice if the phantom is reduced below a certain size.
An additional point to consider is that since the over-beam region is roughly constant regardless of beam collimation, the cross-over effect will be greater for smaller beam collimations.






Scan overlap is also possible on helical scans with pitch less than 1.
The interaction between pitch below 1 and peak surface dose was not explored during this project.
As a result, this model should be used conservatively in such situations.



A very conservative upper limit for the increased dose in overlapping regions is twice the dose predicted by the model.
Based on the dose profile in figure~\ref{f:overlap} and observations made during the course of this research project, I estimate that the effect of scan overlap would typically increase the peak dose by a factor of approximately 1.5.







\subsection{Inter-variable dependence}


One of the foundations of this model-based approach to describing skin dose is separating the effect of the various scan parameters and device characteristics.
In reality, this is a significant simplification.
Many elements of radiation transport depend on beam energy in a manner more complex than a linear fit parameter can describe.


%For example, beam energy will have diverse effects on the interplay between the other parameters.
%For a specific example of this interdependence, beam energy will directly impact the effect of scan length.
%Increasing the peak energy will increase the prevalence of Compton scattering and the penetration of scattered photons.
%This will lead to contribution to surface dose from greater distances.
%However, given the inherent experimental uncertainty in the use of radiochromic film fully describing this dependence would have required a dramatic increase in total experimental runs.


One potential avenue for exploring inter-variable dependence is Monte Carlo simulation, as accurate, reproducible results would allow these dependencies to be investigated and quantified.
However, the combination of complex geometry and need for accurate validation make using Monte Carlo simulation challenging and certainly beyond the scope of this study.



%\subsection{Device specific depth dose}
%
%Depth dose is a device characteristic that primarily depends on filtration and peak energy.
%The thickness and composition of the beam filters affect the beam spectrum which in turn affects the attenuation of the beam passing through the phantom.
%Ideally, any numerical calculations would have been performed using device specific depth dose data.
%However, due to the inaccessibility of manual exposures on the GE Optima 660 it was not possible to measure depth dose experimentally.


\subsection{Bowtie filter profile}

The bowtie filter profile was measured in an extremely limited manual mode that only allowed a 1~mm beam collimation at the isocentre.
Using this manual exposure, it was possible to produce the bowtie filter profiles shown in figure~\ref{f:bowtie}.
However, these profiles required significant post processing and remain uneven in places.
A wider beam would improve the signal to noise ratio and allow the profile to be measured with improved accuracy and precision.
Amending this issue would require access to a more full-featured manual exposure mode.


\subsection{Table attenuation and geometry}

For the purposes of this study, the table attenuation was not robustly quantified.
In particular, the implementation of table attenuation is purely based on whether the beam does or does not pass through the table.
In reality, the effective path length through the table depends on how close the surface of the phantom is to the beam isocentre.


This phenomena is most obvious as the phantom size decreases.
The dose delivered to the top surface of the phantom is around 15\% higher than the bottom for larger phantom sizes, but increases to more than double for the head phantom.
When the bottom of the phantom is close to the isocentre, there is a wide range of source positions that have an increased path length through the phantom.
At angles around 90$^{\circ}$ the beam can travel almost tangentially to the table surface.

As with many aspects of CT geometry, the increased attenuation due to the table at smaller phantom sizes could be accounted for accurately with Monte Carlo simulation.

%\subsection{Helical overscan position}

\subsection{Tube current modulation}

Tube current modulation is a technique used to optimise dose.
When this setting is enabled, the tube current is modulated based on the patient attenuation at each point.
Essentially, this acts to change the dose delivered to each section of the patient.
Despite this, the CT scanner typically only records a single averaged value for CTDIvol.
The effect of tube current modulation would be difficult to quantify robustly, as it would require in vivo measurements on a variety of patients.



\subsection{Patient positioning}

Patient position within the gantry has a significant impact on dose distribution.
The model developed during this thesis assumes the patient or phantom is properly centred within the CT gantry.
Any discrepancy from proper positioning will undermine the predictive power of the model.





\section{Validation on anthropomorphic phantom}
\label{sv:anthro}
While in vivo measurements were not feasible within the scope of this study, an assessment of the model validity in a clinical environment was performed on an anthropomorphic phantom.
In addition to testing the model outside the regime it was developed in, the anthropomorphic phantom allowed a preliminary investigation into the effect of heterogeneity and tube current modulation on peak surface dose.



\subsection{Experiment arrangement}
Long strips of radiochromic film were exposed on the surface of Esteban Atom, an anthropomorphic phantom.
Esteban Atom was a CIRS Atom 701 adult male phantom.


\begin{figure}[t]
	\centering
	\includegraphics[width=.85\linewidth]{dat/pics/anthro.jpg}
	\caption{Experimental arrangement of film on the anterior surface of an anthropomorphic phantom.}
	\label{f:anthro_pic}
\end{figure}

All scans were performed using 40~mm beam collimation and 120~kVp energy.
For the first scan, film was placed on the anterior and posterior sides of the phantom and exposed using a tube current of 200~mA.
For the second scan, film was placed on the anterior, posterior and lateral sides of the phantom and exposed using a modulated tube current.
For the final scan, film was placed on the anterior surface of the phantom and exposed in helical mode using a tube current of 200~mA.
The position of film on the anterior surface of the phantom is shown in figure~\ref{f:anthro_pic}

For each arrangement, a dose estimate for the anterior surface was performed using the stated scanning parameters and the size of the anthropomorphic phantom.
At the chest, the phantom had a 32~cm lateral diameter and 23~cm AP diameter.

\subsection{Dose profiles}

Surface dose profiles acquired on an anthropomorphic phantom are shown in figure~\ref{f:anthro}.
The predicted peak surface dose for the axial and helical scans at constant tube current show reasonable agreement with the measured profiles; these predictions are within tolerance for diagnostic dose estimation.
Tube current modulation, on the other hand, causes the surface dose to vary significantly. The agreement between measured and predicted peak surface dose in this scenario is less promising.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{dat/val/anthro.eps}
	\caption{Surface dose profile measured on an anthropomorphic phantom. The predicted PSD is the anterior surface. Distance is set to 0 at the phantom's neck, and increases in the inferior direction to the waist.
	Upper: Constant tube current.
	Middle: Tube current modulation enabled.
	Lower: Helical mode with constant tube current.}
	\label{f:anthro}
\end{figure}

The constant tube current profiles show that anterior dose depends only weakly on phantom attenuation, with only a slight decrease in dose delivered to the low attenuation regions around the lungs.
On the other hand, posterior dose increases significantly in comparison to anterior dose around these regions.
This implies that the assumption of uniform attenuation used for dose prediction is reasonable for the anterior surface.


The effect of tube current modulation on the surface dose is demonstrated by the central profile in figure~\ref{f:anthro}.
Around the chest the tube current drops dramatically.
This creates the low dose region at 10-20~cm.
Around the abdomen the tube current is increased to counteract the increased attenuation in this region.
As a result, the predicted PSD is higher than the measured PSD around the chest, but lower around the abdomen.
Compensating for this effect with precision is beyond the scope of this project.
Based on the profile measured on the anthropomorphic phantom, I estimate the effect of tube current modulation to be a 50\% increase in peak surface dose for highly heterogeneous scans.


The dose delivered to the lateral sides of the phantom was consistently between the posterior and anterior dose.
This implies that the anterior surface will receive the greatest dose for typical CT scans, even with tube current modulation enabled.








%Usefulness in describing results by other authors
%DeLas Heras
\section{de las Heras data set}
\label{sv:delas}
Of the few articles published on the topic of measuring the relationship between PSD and CTDIvol, only the study by \citeauthor{DelasHeras2013} reported all the scan parameters required by my model.
Their work involved measuring the relationship between CTDIvol and PSD on four CT devices.
The relationship was measured using their standard head scan protocol for each device.
Using the supplied scan parameters, table~\ref{t:delas_comparison} shows a comparison of these published values to predictions calculated using this model.
The predicted PSD/CTDIvol fall within 10\% of the measured value for each device.

The model correctly predicts that the Toshiba protocol will result in the highest PSD/CTDIvol, followed by the Philips model.
Additionally, these predictions were performed without measuring the bowtie filter profile, depth dose curve, or any of the fitted parameters used for measuring the effect of scan length and beam collimation.
Overall, this comparison is promising but inconclusive; controlled testing on additional CT scanners would allow a more thorough assessment of the model.


\begin{table}
	\centering
	\caption{PSD/CTDIvol as measured by \citeauthor{DelasHeras2013}\cite{DelasHeras2013} compared to PSD predicted using the model developed in this thesis.}
	\label{t:delas_comparison}
	\includespread[template=booktabs,columns = top,file=dat/val/delas.ods,range=A2:D5,coltypes={l}{c}{c}{c}]
\end{table}



\section{Model implementation}
%How a user should go about implementing this model!
\label{sv:implement}
The model can be used as-is by downloading the scripts and data source files, then following the instructions provided in the documentation (appendix~\ref{a:scripts}).
However, for accurate dose estimates the model should be supplied with device-specific source data.
The necessary source data for device-specific estimation is:

\begin{itemize}
	\item Bowtie profile for all filters used
	\item Depth dose profile for all energies used
	\item Surface dose measurements at a cross section of relevant scan parameters
\end{itemize}

Measurement of all these features can be acquired using radiochromic film according to the dosimetry protocol established in chapter~\ref{c:film}.
The bowtie filter profile may be measured using the technique described in chapter~\ref{c:film}.
The depth dose profiles can be measured using either film separated by sheets of solid water, Monte Carlo modelling, or another method from the literature.
The full set of surface measurements acquired during this project are not necessary for fitting the equations, although a wide cross-section of data points is ideal.
At minimum, I suggest at least 3 data points for each beam energy, with minimum repetition of scan lengths and beam collimations.


Finally, the scripts provided in appendix~\ref{a:scripts} may be used to facilitate image processing and parameter fitting.
The appropriate methods for replacing the experimental source data are described in the script documentation.




