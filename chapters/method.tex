% !TeX root = ../main.tex
\label{c:meth}

%Methodology chapter
%In this chapter I will talk about:
%What I want to measure
% and so Why film
%

%Positioning my work

%When used as a 
%Due to the low doses delivered, CT skin dosimetry has been left as a relatively unexplored research area.
%However, while the skin dose delivered by CT was unlikely to reach or even approach the threshold for %skin tissue reactions, 

In this thesis, I will analyse the factors that influence patient skin dose in CT and attempt to outline a method by which such a dose may be estimated.
The ability to estimate skin dose quickly may be of particular use in avoiding radiation dermatitis in lengthy high dose interventional procedures involving multiple modalities.

Previous work by \citeauthor{DelasHeras2013} has measured the relationship between CTDI and peak skin dose~\cite{DelasHeras2013}.
The present study aims to further these efforts in several ways.
Firstly, this study will not be restricted to head exposures.
I will consider the torso and attempt to synthesise a result that applies to patients of arbitrary diameter.
Secondly, I will provide a detailed discussion of the relationship between CTDIvol and describe how each scan parameter contributes to the overall dose.
Finally, I will attempt to outline a measurement protocol that other users may apply in order to estimate CT skin dose.

%The goal of this project is twofold:
%
%\begin{itemize}
%	\item Exhaustively establish the relationship between skin dose, CTDIvol and various scan parameters
%	\item Devise a minimally laborious method for estimating the impact of each scan parameter for a specific CT device
%\end{itemize}
%This duality is reflected in both the overall thesis structure and in the treatment of each individual scan parameter.


This chapter will first outline the governing philosophy of my investigation into CT skin dose.
I will then justify some of the decisions regarding my overarching research method.
These decisions include the choice of radiochromic film as a skin dosimeter, the commitment to expressing the result of these experiments as a series of factors, and the method by which the results will be synthesised into a protocol.

Finally, I will present the investigation procedure and provide an experimental overview.

%As such, this thesis is both a description of my research and a guide for future practitioners.
%Therefore, I will endeavour to go into clear, precise detail regarding technical challenges and experimental method.

\section{Hypothesis}

The central hypothesis of this project is that peak skin dose can be computed as the product of CTDIvol and a series of factors.
This relationship is depicted in equation~\ref{e:hypothesis}, where $k_n$ represents each multiplicative factor.


\begin{equation}
PSD=\text{CTDI}_{\text{vol}} \prod_n \left(k_n\right)
\label{e:hypothesis}
\end{equation}





\section{Design methodology}

The  design methodology for this project is based on several fundamental criteria.
These criteria form the justification for decisions between competing experimental methods.
In particular, the goal of this project is to provide a skin dose estimation method which is:

\begin{itemize}
	\item Able to provide a (reasonably) accurate dose estimation
	\item Simple and fast to apply to additional CT devices
	\item Simple and fast to employ this method in order to estimate individual patient dose
\end{itemize}

The first of these criteria requires that the dose estimation method be accurate.
Estimations which do not approximately reflect reality would be, at best, unusable.
At worst, such estimations may negatively impact patient quality of care.
Therefore, an important aspect of this work is to accurately measure and estimate peak skin dose.


The dose estimation method must be useful.
A dose estimation method which is unapproachable would not be used.
In deference to my vanity, I would like the results of this thesis to be of use to someone, somewhere.
Therefore, the process of setting up the dose estimation protocol for a specific CT device and using these results to estimate individual patient dose should be as painless as possible.

Where the concerns of accuracy and approachability conflict, some compromise is necessary.
In these situations, improvement in accuracy must be carefully weighed against an increase in measurement difficulty.



%Method
\section{Measuring skin dose}

Skin dose may be physically measured using several techniques.
Previous studies have used radiochromic film or thermoluminescent dosimeters (TLDs) to estimate skin dose on patients and phantoms.



While both film and TLDs are capable of providing accurate dose measurements, the equipment burden associated with TLDs is quite high.
The requirement of a specialised scanner, annealing oven, and the TLDs themselves represents a considerable cost that would not be feasible for most imaging departments.
In contrast to the specialised equipment required for TLD measurements, radiochromic film can be analysed using a desktop scanner, which many hospitals already own for document and image digitalisation.

TLD measurement requires careful treatment of the delicate crystals and is by all accounts annoying.
The TLDs must be prepared and analysed in batches, which makes the prospect of performing a small number of measurements daunting.
While film dosimetery involves some laborious film scanning and image manipulation, the total time commitment is typically less than for TLD measurement.
Furthermore, some of the more repetitive image manipulation can be automated.
By making the scripts and tools developed for this project available online I hope to make film dosimetry more approachable for other users.


One alternative to physical dose measurement is Monte Carlo simulation.
Simulation has many advantages over practical measurements... along with a few disadvantages.
Firstly, if the model has been adequately validated it is possible to measure dose in complex geometries and in places it would otherwise be impractical to measure physically.
However, a generalised methodology utilising Monte Carlo modelling would require adjustment and validation for each additional scanner the method is performed on.
Additionally, the geometric complexity of CT would make such an adjustment and validation challenging.
Prescribing Monte Carlo modelling to users who wish to apply the method outlined in this thesis would, in my opinion, be a deterrent sufficient to ensure no users attempt to implement my findings.
For this reason, I have not employed modelling as a primary dose measurement tool.



\section{Dependent variables}


The relationship between CTDIvol and skin dose depends on a wide range of variables.
Some variables are obvious, such as the relationship between reported CTDIvol and physically measured CTDIvol.
Others, such as patient position within the CT device, will have an unpredictable impact on skin dose that must be assessed.

The device reported CTDIvol is calculated based on a variety of scan parameters.
Investigating some of these parameters would be a pointless exercise as they have been robustly quantified.
One example of such a scan parameter is the tube current, which has a linear relationship with both CTDIvol and delivered dose.
Other parameters, such as beam collimation, must be investigated as they have an independent effect on CTDIvol and peak skin dose.


%I will attempt to measure the impact of each variable on skin dose by fixing the other scan parameters.  %todo what a shit sentence


%Using the results of these measurements, I will discuss what form a fitted function that accounts for each parameter might take.



%Each subsequent investigation will, by necessity, build on previous results.
%Ideally, for conceptual simplicity, each variable will be shown as largely independent of the others.
%However, scan parameters may have complex interactions requiring a more nuanced approach.



One alternative to the proposed formalism is a multidimensional lookup table based on measured skin dose for varying scan parameters.
By creating a data set for a given CT device, it would be possible to estimate skin dose using interpolation.
However, tabulation presents several drawbacks when compared to parameterisation.
Firstly, if the form of the equation adequately reflects the physical effect of the parameter there are less data points required to provide a fit.
Secondly, the results of parameterisation are less sensitive to the accuracy of each individual measurement.
Thirdly, extrapolation beyond the limit of lookup table values is usually not particularly accurate.
Despite these drawbacks, tabulation works when the interaction between scan parameters is too complex for any kind of straightforward analysis.
In this sense, lookup tables are my fallback position when a physically appropriate fitted solution proves intractable.


\section{Investigation procedure}



The initial stages of the project involved measuring the dose response of radiochromic film.
This required establishing a consistent and accurate film exposure and scanning protocol.
Exposing the film to a series of known doses and measuring the change in reflectance allowed the creation of a calibration function, which can be used to measure unknown dose to future film samples.


Using the established radiochromic film skin dosimetry protocol I, investigated the impact of scan parameters on skin dose.
For each scan parameter I considered the physical basis of its effect on skin dose. 
An approximate formula-based description of the fundamental process was synthesised and compared to measured skin dose.




The variables assessed include beam collimation, total scan length, phantom size and shape, helical vs axial mode, and beam energy.
Following theory and measurements describing each individual parameter, I synthesised the results into a simplified, coherent protocol.



The minimum necessary number of measurements for each aspect of the protocol were then established and discussed.
This included identifying the tests which have a predictable impact on skin dose and therefore do not merit repeated measurement on additional CT devices.



\begin{comment}
\section{Materials}

\subsection{XR-QA2 GafChromic film}

The XR-QA2 Gafchromic film was developed by International Speciality Products as a tool for measuring dose at diagnostically relevant energies~\cite{Ashland2016}.
The film is composed of a layered structure, including a transparent yellow polyester face, an adhesive layer, an active layer and an opaque white polyester back (fig.~\ref{f:xrqa2}).
Like all radiochromic film, this model contains monomer filaments which, when exposed to the activation energy provided by ionising radiation, polymerise releasing a dye and effecting a colour change in the film~\cite{Devic2011}.
In XR-QA2 film, the active layer is composed of monomer filaments with Bi$_2$O$_3$ added to increase the photoelectric cross section.

The layered structure of the film is shown in figure~\ref{f:xrqa2}. 

\begin{figure}
	\centering
	\includegraphics[width=.35\linewidth]{dat/bk/layers.png}
	\caption{Layered structure of Gafchromic XRQA2 film. \imagecite{Devic2016}}
	\label{f:xrqa2}
\end{figure}


\subsection{Optima Discovery 660}




\subsection{CTDI phantom}

\subsection{RALPH anthropomorphic phantom}

\subsection{Radcal  dosimeter}

\subsection{Document scanner}


\end{comment}





