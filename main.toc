\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {british}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Context}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Research questions}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Thesis objectives}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Novel contributions}{3}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Thesis overview}{3}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Background}{5}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Skin damage in interventional radiology}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Cause of tissue damage}{6}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Radiation dermatitis}{6}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Skin dose in Angio-CT}{8}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}CT dosimetry}{8}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Use of CTDI}{9}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Skin dose in CT}{10}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Radiochromic film dosimetry}{11}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Physical basis}{12}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Film as a skin dosimeter}{13}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Film measurement protocol}{13}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Image filtration}{15}{subsection.2.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.5}Surface measurements}{15}{subsection.2.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.6}Radiochromic film in CT dosimetry}{16}{subsection.2.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Conclusion}{17}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Research framework}{18}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Hypothesis}{19}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Design methodology}{19}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Measuring skin dose}{20}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Dependent variables}{21}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Investigation procedure}{22}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Film dosimetry}{24}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Film dosimetry protocol}{25}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Calibration experimental arrangement}{25}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Scanning technique}{26}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Calibration function}{27}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Uncertainty calculation}{27}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Data processing}{29}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Calibration results}{30}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Film dosimetry discussion}{32}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Measurement uncertainty}{32}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}120 kVp fit}{33}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Calibration range}{34}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Surface measurement coefficient}{34}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.5}Calibration energy}{35}{subsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Conclusions}{36}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Skin dose}{37}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Theory}{38}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Beam energy}{38}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Scan length}{38}{subsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Beam collimation}{39}{subsection.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Patient size and geometry}{40}{subsection.5.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.5}Helical scans}{42}{subsection.5.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Method}{44}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Surface dose measurements}{44}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Consistency}{46}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Function fitting}{46}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.4}Bowtie filter}{46}{subsection.5.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.5}Depth dose}{46}{subsection.5.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.6}Patient size}{48}{subsection.5.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.7}Helical scans}{50}{subsection.5.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Results}{50}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Surface dose}{50}{subsection.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Consistency}{51}{subsection.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3}Beam collimation and scan length}{51}{subsection.5.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.4}Bowtie filter profile}{55}{subsection.5.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.5}Depth dose profiles}{56}{subsection.5.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.6}Patient size results}{57}{subsection.5.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.7}Helical results}{58}{subsection.5.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.8}Explanatory power}{62}{subsection.5.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Model summary}{64}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.1}Model formalism}{64}{subsection.5.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.2}Conclusions}{64}{subsection.5.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Model assessment}{66}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Limitations of the model}{67}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.1}Beam hardening}{67}{subsection.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.2}Scan overlap}{68}{subsection.6.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.3}Inter-variable dependence}{70}{subsection.6.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.4}Bowtie filter profile}{70}{subsection.6.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.5}Table attenuation and geometry}{71}{subsection.6.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.6}Tube current modulation}{71}{subsection.6.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.7}Patient positioning}{72}{subsection.6.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Validation on anthropomorphic phantom}{72}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.1}Experiment arrangement}{72}{subsection.6.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.2}Dose profiles}{73}{subsection.6.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}de las Heras data set}{75}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Model implementation}{76}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Conclusion}{78}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Further work}{79}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.1}Implementation on additional CT scanners}{79}{subsection.7.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.2}in vivo dosimetry}{79}{subsection.7.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.3}Automated dosimetry}{79}{subsection.7.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.4}Monte Carlo modelling}{80}{subsection.7.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Dosimetry protocol}{90}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Scripts}{91}{appendix.B}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.1}Film processing script}{91}{section.B.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B.2}Dose calculation script}{92}{section.B.2}
